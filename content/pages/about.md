---
title: ""
draft: false
---

# ABOUT ME

Moi c’est Draconis geek depuis que je suis tombé dans la marmite. Je suis passionné d’informatique, de nouvelles technologies, de jeux-vidéos et de la culture geek en générale. Lorsque je ne suis pas sur mon clavier, je suis derrière mon objectif photo essayant de capturer ma vision du monde. Je suis un fervent partisan du fais le toi-même, tu possèdes la connaissance, les compétences ou simplement l’envie, pourquoi se priver ? Utilisateur de Linux depuis 1998, je ne suis pas un extrémiste du libre, chacun utilise l’informatique comme il l’entend et en fonction des ses besoins. Je vous laisse me découvrir à travers mes deux moyens d'expressions mon blog et mes photos.

# BLOG

Ce blog est un moyen pour moi de relier mes différents centres d’intérêts et passions. Et surtout le plus important pour moi, de partager, sans le partage nous ne sommes rien. Il me sert de bloc-notes, de mémos, à m'exprimer librement, partager mes passions ou d'en discuter autour d'une bonne bière. Ici on parlera, jeux-vidéos, linux, figurines, DIY, et plus si affinité.
Ce blog est entièrement auto-hébergé, preuve que cela fonctionne, il est en ligne depuis mars 2011. Pour la technique il s'agit d'un container fonctionnant sur LXC avec DEBIAN comme OS principal. Il est fièrement propulsé par Hugo, Nginx, Let's encrypt et votre serviteur !

# MENTIONS LEGALES

Sauf mentions contraires, je suis l’unique auteur du contenu de ce blog et je me réserve un droit total sur tout son contenu. En postant un commentaire, vous acceptez donc qu’il puisse être retiré ou édité sans préavis ni recours légitime.
Ce blog utilise n'utilise aucun cookies ou outils de pistage. Je n'aime pas être pisté par les sites que je visite donc je respecte cet adage : Ne fais pas aux autres ce que tu ne voudrais pas qu'on te fasse. J'utilise uniquement [Goacccess](https://goaccess.io/) pour voir un peu la fréquentation du blog.
Sauf indications contraires le contenu de se site est mis à disposition selon les termes de la CC BY-NC-SA 4.0. Pour utiliser son contenu :

* vous devez me créditer, intégrer un lien vers la page en question et indiquer si des modifications ont été effectuées.
* vous n’êtes pas autorisé à faire un usage commercial du contenu de ce site.
* vous devez diffuser votre contenu dans les mêmes conditions, que vous ayez effectué des modifications ou non au contenu original.